#include <iostream>
#include <cassert>
#include <luaglue/luaglue.hpp>

static void print(void)
{
    std::cout << "hello world" << std::endl;
}

extern "C" int luaopen_scope(lua_State *L)
{
    // export to _G
    luaglue::scope(L)
    [
        luaglue::def("print_G", print)
    ];

    // export to _G["scope"]
    luaglue::scope(L, "scope")
    [
        luaglue::def("print_scope", print)
    ];

    // another way
    luaglue::object s(L, "scope"); // hold the lua table _G.scope
    assert(LUA_TTABLE == s.type());
    luaglue::scope(s)
    [
        luaglue::def("another_way", print)
    ];

    return 0;
}

